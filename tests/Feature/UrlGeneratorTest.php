<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\URL;
use Illuminate\Testing\TestResponse;
use Illuminate\Contracts\Http\Kernel as HttpKernel;

class UrlGeneratorTest extends TestCase
{
    /** @test */
    function generate_urls_using_trailing_slashes(): void
    {
        self::assertSame('http://localhost/some-route/', url('some-route/'));
        self::assertSame('http://localhost/some-route/', Url::to('some-route/'));
    }

    /** @test */
    function enable_trailing_slashes_globally(): void
    {
        config(['optimized-seo-url-generator.global' => true]);

        self::assertSame('http://localhost/some-route/', url('some-route'));
    }

    /** @test */
    function generate_urls_following_route_definition_if_enabled(): void
    {
        config(['optimized-seo-url-generator.global' => false]);
        config(['optimized-seo-url-generator.generate-urls-matching-route-definitions' => true]);

        // Route::get('route-with-trailing-slash', ...)->name('route.with.trailing.slash')
        self::assertSame('http://localhost/route-with-trailing-slash/', url('/route-with-trailing-slash'));
        self::assertSame('http://localhost/route-without-trailing-slash', url('/route-without-trailing-slash/'));
    }

    /** @test */
    function generate_urls_with_trailing_slashes_and_query_strings(): void
    {
        config(['optimized-seo-url-generator.global' => false]);
        config(['optimized-seo-url-generator.generate-urls-matching-route-definitions' => true]);

        self::assertSame(
            'http://localhost/route-with-trailing-slash/?foo=1&bar=2',
            url('/route-with-trailing-slash?foo=1&bar=2')
        );
    }

    /** @test */
    function generate_urls_as_defined_in_routes_using_the_route_helper(): void
    {
        self::assertSame('http://localhost/route-with-trailing-slash/', route('route.with.trailing.slash'));
        self::assertSame('http://localhost/route-without-trailing-slash', route('route.without.trailing.slash'));
    }

    /** @test */
    function generate_urls_as_defined_in_routes_with_required_route_parameters(): void
    {
        self::assertSame(
            'http://localhost/trailing-slash-and-required-parameter/required-param/',
            route('trailing.slash.and.required.parameter', 'required-param')
        );

        self::assertSame(
            'http://localhost/no-trailing-slash-and-required-parameter/required-param',
            route('no.trailing.slash.and.required.parameter', 'required-param')
        );
    }

    /** @test */
    function generate_urls_as_defined_in_routes_with_optional_route_parameters(): void
    {
        self::assertSame(
            'http://localhost/trailing-slash-and-optional-parameter/optional-param/',
            route('trailing.slash.and.optional.parameter', 'optional-param')
        );

        self::assertSame(
            'http://localhost/no-trailing-slash-and-optional-parameter/optional-param',
            route('no.trailing.slash.and.optional.parameter', 'optional-param')
        );

        self::assertSame(
            'http://localhost/trailing-slash-and-optional-parameter/',
            route('trailing.slash.and.optional.parameter')
        );

        self::assertSame(
            'http://localhost/no-trailing-slash-and-optional-parameter',
            route('no.trailing.slash.and.optional.parameter')
        );
    }

    /** @test */
    function optional_multi_level_parameter(): void
    {
        self::assertSame('http://localhost/category/', route('paginated.category'));

        $this->getJson('/category')
            ->assertJson([]);

        $this->getJson('/category/')
            ->assertJson([]);

        $this->getJson('/category/page/2')
            ->assertJson([
                'page' => 'page/2'
            ]);

        $this->getJson('/category/page/2/')
            ->assertJson([
                'page' => 'page/2'
            ]);
    }

    /** @test */
    function redirect_based_on_route_definition(): void
    {
        config(['optimized-seo-url-generator.generate-urls-matching-route-definitions' => true]);
        config(['optimized-seo-url-generator.redirect_status' => 302]);

        $this->makeRequest('middleware-with-trailing-slash/')->assertOk();
        $this->makeRequest('/middleware-without-trailing-slash')->assertOk();

        $this->makeRequest('/middleware-with-trailing-slash')
            ->assertStatus(302)
            ->assertRedirect('/middleware-with-trailing-slash/');

        $this->makeRequest('/middleware-without-trailing-slash/')
            ->assertStatus(302)
            ->assertRedirect('/middleware-without-trailing-slash');
    }

    protected function makeRequest($path, $method = 'GET', $arguments = []): TestResponse
    {
        $kernel = app()->make(HttpKernel::class);

        $response = $kernel->handle(
            app('request')->create($path, $method, $arguments)
        );
        return TestResponse::fromBaseResponse($response);
    }
}
