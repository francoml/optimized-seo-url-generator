<?php

namespace Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use Square1\OptimizedSeoUrlGenerator\UrlGeneratorServiceProvider;

class TestCase extends BaseTestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            UrlGeneratorServiceProvider::class,
            TestRoutesServiceProvider::class,
        ];
    }
}