<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Square1\OptimizedSeoUrlGenerator\TrailingSlashRedirectionMiddleware;

Route::get('/category/{page?}/', function (Request $request) {
    return response()->json([
        'page' => $request->route('page')
    ]);
})->where('page', 'page\/[\d]+')
    ->name('paginated.category');

Route::get('/news/{page?}', function (Request $request) {
    return response()->json([
        'page' => $request->route('page')
    ]);
})->where('page', 'page\/[\d]+')
    ->name('paginated.route');

Route::get('route-with-trailing-slash/', function () {
    return response('ok');
})->name('route.with.trailing.slash');

Route::get('route-without-trailing-slash', function () {
    return response('ok');
})->name('route.without.trailing.slash');

Route::get('trailing-slash-and-required-parameter/{slug}/', function () {
    return response('ok');
})->name('trailing.slash.and.required.parameter');

Route::get('no-trailing-slash-and-required-parameter/{slug}', function () {
    return response('ok');
})->name('no.trailing.slash.and.required.parameter');

Route::get('trailing-slash-and-optional-parameter/{slug?}/', function () {
    return response('ok');
})->name('trailing.slash.and.optional.parameter');

Route::get('no-trailing-slash-and-optional-parameter/{slug?}', function () {
    return response('ok');
})->name('no.trailing.slash.and.optional.parameter');

Route::middleware(TrailingSlashRedirectionMiddleware::class)->group(function () {
    Route::get('/middleware-with-trailing-slash/', function () {
        return response('ok');
    });

    Route::get('/middleware-without-trailing-slash', function () {
        return response('ok');
    });
});