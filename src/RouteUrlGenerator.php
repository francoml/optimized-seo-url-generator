<?php

namespace Square1\OptimizedSeoUrlGenerator;

use Illuminate\Routing\Exceptions\UrlGenerationException;
use Illuminate\Routing\RouteUrlGenerator as BaseRouteUrlGenerator;

class RouteUrlGenerator extends BaseRouteUrlGenerator
{
    public function to($route, $parameters = [], $absolute = false)
    {
        $domain = $this->getRouteDomain($route, $parameters);

        $uri = $this->addQueryString(
            $this->url->format(
                $root = $this->replaceRootParameters($route, $domain, $parameters),
                $this->replaceRouteParameters($route->uri(), $parameters),
                $route
            ),
            $parameters
        );

        if (preg_match_all('/{(.*?)}/', $uri, $matchedMissingParameters)) {
            throw UrlGenerationException::forMissingParameters($route, $matchedMissingParameters[1]);
        }

        $uri = strtr(rawurlencode($uri), $this->dontEncode);

        if (! $absolute) {
            $uri = preg_replace('#^(//|[^/?])+#', '', $uri);

            if ($base = $this->request->getBaseUrl()) {
                $uri = preg_replace('#^'.$base.'#i', '', $uri);
            }

            return '/'.ltrim($uri, '/');
        }

        return $uri;
    }
}