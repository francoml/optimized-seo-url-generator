<?php

namespace Square1\OptimizedSeoUrlGenerator;

use Illuminate\Http\Request;

class UriValidator
{

    public function matches(Route $route, Request $request)
    {
        $path = rtrim($request->getPathInfo(), '/') ?: '/';

        return preg_match($route->getCompiled()->getRegex(), rawurldecode($path));
    }
}
