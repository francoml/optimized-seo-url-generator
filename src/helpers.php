<?php

use Illuminate\Contracts\Routing\UrlGenerator;

if (! function_exists('finalUrl')) {
    function finalUrl($path = null, $parameters = [], $secure = null)
    {
        if (is_null($path)) {
            return app(UrlGenerator::class);
        }

        return app(UrlGenerator::class)->toFinal($path, $parameters, $secure);
    }
}
