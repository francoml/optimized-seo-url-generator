<?php

namespace Square1\OptimizedSeoUrlGenerator;

use Closure;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class TrailingSlashRedirectionMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($this->shouldRedirect($request)) {
            // Rebuild the URL using $request->getRequestUri() to enforce validation,
            // otherwise it will redirect to the same url without trailing slashes.
            return redirect()->to(URL::to($request->getRequestUri()), config('optimized-seo-url-generator.redirect_status'));
        }
        return $next($request);
    }

    private function shouldRedirect(Request $request): bool
    {
        if ($this->invalidatedByRouteParams($request) ||
            $this->invalidatedByQueryParams($request) ||
            $this->invalidateByRoutePattern($request)) {
            return false;
        }

        if (Str::endsWith($request->route()->uri, '/') && !Str::endsWith($request->getPathInfo(), '/')) {
            return true;
        }

        if (config('optimized-seo-url-generator.generate-urls-matching-route-definitions')) {
            $routeTrailingSlash = $request->route()->isFinal() ? '' : '/';
            $currentTrailingSlash = Str::endsWith($request->getPathInfo(), '/') ? '/' : '';

            return $routeTrailingSlash !== $currentTrailingSlash;
        }

        return false;
    }

    private function invalidatedByRouteParams(Request $request): bool
    {
        if (empty ($request->route()->parameters())) {
            return false;
        }

        return count(
            array_intersect(
                config('optimized-seo-url-generator.redirection.route-param-invalidation', []),
                array_keys($request->route()->parameters())
            )
        ) > 0;
    }

    private function invalidatedByQueryParams(Request $request): bool
    {
        return count(
            array_intersect(
                config('optimized-seo-url-generator.redirection.query-param-invalidation', []),
                array_keys($request->query())
            )
        ) > 0;
    }

    private function invalidateByRoutePattern(Request $request): bool
    {
        foreach (config('optimized-seo-url-generator.redirection.route-pattern-invalidation', []) as $pattern) {
            if ($request->is($pattern)) {
                return true;
            }
        }

        return false;
    }
}
