<?php

namespace Square1\OptimizedSeoUrlGenerator;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Routing\Route as BaseRoute;
use Illuminate\Routing\Matching\HostValidator;
use Illuminate\Routing\Matching\SchemeValidator;
use Illuminate\Routing\Matching\MethodValidator;

class Route extends BaseRoute
{
    protected bool $final;

    public function __construct($methods, $uri, $action)
    {
        $this->final(Str::endsWith($uri, '/'));
        $this->originalUri = $uri;
        BaseRoute::__construct($methods, $uri, $action);
    }

    public function final(bool $final): self
    {
        $this->final = $final;
        return $this;
    }

    public function isFinal(): bool
    {
        return $this->final || config('optimized-seo-url-generator.global');
    }

    public function prefix($prefix): self
    {
        $prefix = $prefix ?? '';

        $this->updatePrefixOnAction($prefix);

        $uri = rtrim($prefix, '/').'/'.ltrim($this->uri, '/');

        return $this->setUri($uri !== '/' ? ltrim($uri, '/') : $uri);
    }

    public static function getValidators(): array
    {
        if (isset(static::$validators)) {
            return static::$validators;
        }

        // To match the route, we will use a chain of responsibility pattern with the
        // validator implementations. We will spin through each one making sure it
        // passes and then we will know if the route as a whole matches request.
        return static::$validators = [
            new UriValidator,
            new MethodValidator,
            new SchemeValidator,
            new HostValidator,
        ];
    }

    public function matches(Request $request, $includingMethod = true): bool
    {
        $this->compileRoute();

        foreach (self::getValidators() as $validator) {
            if (! $includingMethod && $validator instanceof MethodValidator) {
                continue;
            }

            if (! $validator->matches($this, $request)) {
                return false;
            }
        }

        return true;
    }
}