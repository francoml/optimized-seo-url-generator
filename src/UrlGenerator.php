<?php

namespace Square1\OptimizedSeoUrlGenerator;

use Illuminate\Support\Str;
use Illuminate\Routing\UrlGenerator as BaseUrlGenerator;

class UrlGenerator extends BaseUrlGenerator
{
    protected function routeUrl()
    {
        if (! $this->routeGenerator) {
            $this->routeGenerator = new RouteUrlGenerator($this, $this->request);
        }

        return $this->routeGenerator;
    }

    public function to($path, $extra = [], $secure = null)
    {
        $originalPath = $path;

        if ($this->isValidUrl($path)) {
            return $path;
        }

        $tail = implode('/', array_map('rawurlencode', (array) $this->formatParameters($extra)));

        $root = $this->formatRoot($this->formatScheme($secure));


        [$path, $query] = $this->extractQueryString($path);

        $formatted = $this->format(
            $root, '/'.trim($path.'/'.$tail, '/')
        );

         //START
        if  (config('optimized-seo-url-generator.generate-urls-matching-route-definitions')) {
            $formatted = rtrim($formatted, '/') . $this->getTrailingSlashFromRouteDefinition($path);
        } elseif (Str::endsWith($originalPath, '/')) {
            $formatted = rtrim($formatted, '/') . '/';
        }
         //END

        return $formatted.$query;
    }

    public function format($root, $path, $route = null): string
    {
        $url = rtrim(parent::format($root, $path, $route), '/');

        if  (config('optimized-seo-url-generator.generate-urls-matching-route-definitions')) {
            return rtrim($url, '/') . $this->getTrailingSlashFromRouteDefinition($path);
        }

        if (config('optimized-seo-url-generator.global')) {
            return rtrim($url, '/') . '/';
        }

        if (empty($route)) {
            return $url .( Str::endsWith($path, '/') ? '/' : '');
        }

        return rtrim($url, '/') . ($route->isFinal() ? '' : '/');
    }

    public function toFinal($path, $extra = [], $secure = null): string
    {
        $url = $this->to($path, $extra, $secure);

        $base = Str::before($url, '?');

        if (Str::endsWith($base, '/')) {
            return trim($base, '/') . Str::after($url, $base);
        }

        return $url;
    }

    private function getTrailingSlashFromRouteDefinition($path): string
    {
        try {
            foreach ($this->routes as $route) {
                if (preg_match($route->toSymfonyRoute()->compile()->getRegex(), '/'.trim($path, '/'))) {
                    return $route->isFinal() ? '' : '/';
                }
            }
            return '';
        } catch (\Throwable $e) {
            return '';
        }
    }
}
