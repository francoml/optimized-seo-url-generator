<?php

namespace Square1\OptimizedSeoUrlGenerator;

use Illuminate\Routing\RoutingServiceProvider;

class UrlGeneratorServiceProvider extends RoutingServiceProvider
{
    public function boot(): void
    {
        Route::macro('final', function () {
            $this->isFinal = true;
            return $this;
        });
    }

    public function register(): void
    {
        parent::register();
        $this->registerPublishableAssets();
    }

    public function registerUrlGenerator(): void
    {

        $this->app->singleton('url', function ($app) {
            $routes = $app['router']->getRoutes();

            // The URL generator needs the route collection that exists on the router.
            // Keep in mind this is an object, so we're passing by references here
            // and all the registered routes will be available to the generator.
            $app->instance('routes', $routes);

            $url = new UrlGenerator(
                $routes,
                $app->rebinding(
                    'request',
                    $this->requestRebinder()
                ),
                $app['config']['app.asset_url']
            );

            // Next we will set a few service resolvers on the URL generator so it can
            // get the information it needs to function. This just provides some of
            // the convenience features to this URL generator like "signed" URLs.
            $url->setSessionResolver(function () use ($app) {
                return $app['session'] ?? null;
            });

            $url->setKeyResolver(function () use ($app) {
                return $app->make('config')->get('app.key');
            });


            // If the route collection is "rebound", for example, when the routes stay
            // cached for the application, we will need to rebind the routes on the
            // URL generator instance so it has the latest version of the routes.
            $app->rebinding('routes', function ($app, $routes) {
                $app['url']->setRoutes($routes);
            });

            return $url;
        });
    }

    protected function registerRouter(): void
    {
        $this->app->singleton('router', function ($app) {
            return new \Square1\OptimizedSeoUrlGenerator\Router($app['events'], $app);
        });
    }

    private function registerPublishableAssets(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/optimized-seo-url-generator.php' => config_path('/optimized-seo-url-generator.php'),
            ], 'optimized-seo-url-generator-config');
        }
    }
}
