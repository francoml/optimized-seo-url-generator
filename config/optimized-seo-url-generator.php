<?php

return [
    /**
     * Forces the Url::get(), route(), Route::to(), url() ... to return
     * URLs with the trailing slash by default
     */
    'global' => false,

    /**
     * This can slows down your application.
     * Everytime you use the url('') helper, or the Url::to() method
     * The url-generator will check if a route for the given pattern
     * was defined in the route files with a trailing slash, if so,
     * then will return the URL with the trailing slash.
     */
    'generate-urls-matching-route-definitions' => true,

    'redirect_status' => 302,

    /**
     * Avoid redirecting to the URL with trailing slashes if the
     * TrailingSlashRedirectionMiddleware::class is enabled and any
     * of the query/route parameters are present in the request.
     */
    'redirection' => [
        'route-param-invalidation' => ['page'],
        'query-param-invalidation' => ['page'],
        'route-pattern-invalidation' => [
            //    'category/*'
        ]
    ]
];