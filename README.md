# Laravel SEO optimized URL generator

This package adds a trailing slash to any URL generated in the application, but also includes a few new methods to generate URLs without trailing slashes if needed.

## Install

Using composer

```bash
composer require square1/optimized-seo-url-generator
```

After install, register the service provider in the `/bootstrap/app.php` file:

```php
$app->register(\Square1\OptimizedSeoUrlGenerator\UrlGeneratorServiceProvider::class);
```

## Usage

The recommended usage of this package is to add the trailing slashes as part of your routes definitions in the routes files.

For example:

```php
// With trailing slash
Route::get('/{category}/', ...)->name('category');
Route::get('/news/', ...)->name('news');

// W/o trailing slash
Route::get('/news/{slug}-{id}', ...)->name('news.article');
```

When you generate a new route using the `Route::to()` or `route()` helper, you'll get the route with the trailing slash:

```php
route('category', ['category' => 'sports']); // http://localhost/sports/
route('news'); // http://localhost/news/
route('news.article', ['slug' => 'my-first-article', 'id' => '123']); // http://localhost/my-first-article-123
```

You can use this as well to generate urls using `Url::to()` or `url()` if the `generate-urls-matching-route-definitions` option is active in the config file:

```php
<?php
    return [
        'generate-urls-matching-route-definitions' => true
    ];
```

This will check if there is a matching route for the given path, and it will add the trailing slash if the route definition has it.

```php
// routes/web.php
Route::get('/{category}/', ...)->name('category');


url('/sports'); // http://localhost/sports/ 
```

> In this case, each call to the `url()` helper function will trigger a check to the entire routes list, so it is recommended to use the route() helper instead.

## URL redirection

To enforce a redirection to a URL with the trailing slash, use the `TrailingSlashRedirectionMiddleware::class` as follows:

```php
Route::any('/', function () {
    //
})->middleware(TrailingSlashRedirectionMiddleware::class);
```

Or add it globally to your `Kernel.php` file.

## Advanced configuration

You can mix a lot of these settings, although in most cases you won't need to change the default configuration.

### Enable globally

To enforce the URL generation using trailing slashes with the `url()` helper, turn on the following option in the config file:

```php
<?php

return [
    'global' => false,
];
```

### Avoid URL redirection for certain URLS

You can invalidate the redirection in three different ways, first you'd need to publish the config file:

```bash
php artisan vendor:publish
```

Then you can select to invalidate by `query-param`, 'route-param`, or `route-pattern`

```php
<?php

return [
    /**
     * Avoid redirecting to the URL with trailing slashes if the
     * TrailingSlashRedirectionMiddleware::class is enabled and any
     * of the query/route parameters are present in the request.
     */
    'redirection' => [
        'route-param-invalidation' => ['page'],
        'query-param-invalidation' => ['page'],
        'route-pattern-invalidation' => [
            //    'category/*'
        ]
    ]
];
``` 

